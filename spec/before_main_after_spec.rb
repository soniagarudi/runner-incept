require 'rspec'
require_relative '../tanuki/tanuki.rb'

tanuki = Tanuki.new

describe "Shell executor script steps" do
    it "executes before, main, and after scripts" do
        trace = tanuki.get_job_trace('shell-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
end

describe "Docker executor script steps" do
    it "executes before, main, and after scripts with default shell" do
        trace = tanuki.get_job_trace('docker-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
    it "executes before, main, and after scripts with power shell" do
        trace = tanuki.get_job_trace('pwsh-docker-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
end

describe "Custom executor script steps" do
    it "executes before, main, and after scripts" do
        trace = tanuki.get_job_trace('custom-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
end

describe "Windows docker executor script steps" do
    it "executes before, main, and after scripts" do
        trace = tanuki.get_job_trace('windows-1809-docker-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
end

describe "Windows shell executor script steps" do
    it "executes before, main, and after scripts" do
        trace = tanuki.get_job_trace('windows-1809-cmd-hello-world')
        expect(trace).to include('this is the before script')
        expect(trace).to include('this is the main script')
        expect(trace).to include('this is the after script')
    end
end
